Methods = {}

function Methods.SetRace(targetPid, newRace)
	if logicHandler.CheckPlayerValidity(pid, targetPid) then
		Players[targetPid].data.character.race = newRace
		tes3mp.SetRace(targetPid, newRace)
		tes3mp.SetResetStats(targetPid, false)
		tes3mp.SendBaseInfo(targetPid)
	end
end

function Methods.SetHead(targetPid, newHead)
	if logicHandler.CheckPlayerValidity(pid, targetPid) then
		Players[targetPid].data.character.head = newHead
		tes3mp.SetHead(targetPid, newHead)
		tes3mp.SetResetStats(targetPid, false)
		tes3mp.SendBaseInfo(targetPid)
	end
end

function Methods.SetHair(targetPid, newHair)
	if logicHandler.CheckPlayerValidity(pid, cmd[2]) then
		Players[targetPid].data.character.hair = newHair
		tes3mp.SetHair(targetPid, newHair)
		tes3mp.SetResetStats(targetPid, false)
		tes3mp.SendBaseInfo(targetPid)
	end
end

function Methods.SetAttribute(targetPid, attrId, value)
	if logicHandler.CheckPlayerValidity(pid, targetPid) then
		if tonumber(attrId) ~= nil and tonumber(value) ~= nil then
			attrId = tes3mp.GetAttributeId(attrId)
		    
			if attrId ~= -1 and attrId < tes3mp.GetAttributeCount() then
				tes3mp.SetAttributeBase(targetPid, attrId, value)
				tes3mp.SendAttributes(targetPid)

				local message = "// " .. Players[targetPid].chat.accountName .. tr("'s ") .. "#ff0000" .. tes3mp.GetAttributeName(attrId) ..
				tr(" is now ") .. value .. "\n"
				Players[pid]:Message(message)
				Players[targetPid]:SaveAttributes()
		    end
		end
	end
end

function Methods.SetSkill(targetPid, skillId, value)
	if logicHandler.CheckPlayerValidity(pid, targetPid) then
		if tonumber(skillId) ~= nil and tonumber(value) ~= nil then
			skillId = tes3mp.GetSkillId(skillId)

		    if skillId ~= -1 and skillId < tes3mp.GetSkillCount() then
		tes3mp.SetSkillBase(targetPid, skillId, value)
		tes3mp.SendSkills(targetPid)

		local message = "// " .. Players[targetPid].chat.accountName .. tr("'s ") .. "#ff0000" .. tes3mp.GetSkillName(skillId) ..
				tr(" is now ") .. value .. "\n"
		Players[pid]:Message(message)
		Players[targetPid]:SaveSkills()
		    end
		end
	end
end

function Methods.SetScale(pid, targetPid, scale)
	if logicHandler.CheckPlayerValidity(pid, targetPid) then
		if tonumber(scale) == nil then		    
		     Players[pid]:Message("// #ff0000" .. tr("Not a valid argument. Use") .. " /setscale <pid> <value>.\n")
		     return
		end

		Players[targetPid]:SetScale(scale)
		Players[targetPid]:LoadShapeshift()
		Players[pid]:Message("// #ff0000" .. tr("Scale for ") .. Players[targetPid].chat.accountName .. "#ff0000" .. tr(" is now ") .. scale .. "\n")
		if targetPid ~= pid then
		    Players[targetPid]:Message("// #ff0000" .. tr("Your scale is now ") .. scale .. "\n")
		end
	end
end

function Methods.SetWerewolf(pid, targetPid, command)
	if logicHandler.CheckPlayerValidity(pid, targetPid) then
		if command == "on" then
		    Players[targetPid]:SetWerewolfState(true)
		    state = tr(" enabled.\n")
		elseif command == "off" then
		    Players[targetPid]:SetWerewolfState(false)
		    state = tr(" disabled.\n")
		else
		     Players[pid]:Message("// #ff0000" .. tr("Not a valid argument. Use") .. " /setwerewolf <pid> on/off.\n")
		     return
		end

		Players[targetPid]:LoadShapeshift()
		Players[pid]:Message("// #ff0000" .. tr("Werewolf state for ") .. Players[targetPid].chat.accountName .. "#ff0000" .. state)
		if targetPid ~= pid then
		    Players[targetPid]:Message("// #ff0000" .. tr("Werewolf state ") .. "#ff0000" .. state)
		end
	end
end

function Methods.Disguise(pid, targetPid, creatureRefId)
	if logicHandler.CheckPlayerValidity(pid, targetPid) then
	    Players[targetPid].data.shapeshift.creatureRefId = creatureRefId
	    tes3mp.SetCreatureRefId(targetPid, creatureRefId)
	    tes3mp.SendShapeshift(targetPid)

	    if creatureRefId == "" then
			creatureRefId = tr("nothing")
	    end

	    Players[pid]:Message("// " .. Players[targetPid].chat.accountName .. "#ff0000" .. tr(" is now disguised as ") .. creatureRefId .. "\n")
	    if targetPid ~= pid then
			Players[targetPid]:Message("// #ff0000" .. tr("You are now disguised as ") .. creatureRefId .. "\n")
	    end
	end
end

function Methods.UseCreatureName(pid, targetPid, command)
	if logicHandler.CheckPlayerValidity(pid, targetPid) then
		local nameState = false

		if command == "on" then
		    nameState = true
		elseif command == "off" then
		    nameState = false
		else
		     Players[pid]:Message("// #ff0000" .. tr("Not a valid argument. Use") .. " /usecreaturename <pid> on/off\n")
		     return
		end

		Players[targetPid].data.shapeshift.displayCreatureName = nameState
		tes3mp.SetCreatureNameDisplayState(targetPid, nameState)
		tes3mp.SendShapeshift(targetPid)
	end
end

return Methods
