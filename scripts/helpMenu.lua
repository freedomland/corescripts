mpWidgets = require("mpWidgets")
tableHelper = require("tableHelper")
commandInterface = require("commandInterface")

local databuttons = {
	["help player"] = {
		requireStuffRank = 0,
		title = "Помощь игроку"
	},
	
	["help moderator"] = {
		requireStuffRank = 1,
		title = "Помощь модератору"
	},
	
	["help ordinator"] = {
		requireStuffRank = 2,
		title = "Помощь ординатору"
	},
	
	["help ordinator page 2"] = {
		requireStuffRank = 2,
		title = "Помощь ординатору страница 2"
	},
		
	["help admin page 1"] = {
		requireStuffRank = 3,
		title = "Помощь Богу страница 1"
	},
	
	["help admin page 2"] = {
		requireStuffRank = 3,
		title = "Помощь Богу страница 2"
	}
}

local data = {}

data["help player"] = {
	title = "Список комманд",
	commands = {
		{"/message <pid> или \"<ник>\" <текст>", "Отправить приватное сообщение игроку", "(/msg ; /w ; /pm)"},
		{"/me <text>", "Отправить сообщение от третьего лица"},
		{"/a <text>", "Отправить сообщение в глобальный чат"},
		{"/list", "Показать список всех игроков на сервере"},
		{"/anim <animation>", "Проиграть анимацию персонажа"},
		{"/chatopt [local | global]", "Показать окно настроек чата"},
		{"/speech <type> <index>", "Проиграть речь, доступную для этой расы", "(/s)"},
		{"/do <text>", "Сказать что-либо от третьего лица. Например: Убью этого гуара. Эти ботинки всё-равно испорчены."},
		{"/todo <повествование> * <кому>", "Сказать что-либо от третьего лица в повестовании. Например: Иди сюда, раб! * этому каджиту."},
		{"/try <текст>", "Попробовать сделать что-либо. Например: Подбросить монетку."},
		{"/suicide", "Сделать сепуку"},
		{"/fixme", "Выбраться из затруднительного положения"}
	}
}

data["help moderator"] = {
	title = tr("Moderator commands"),
	commands = {
		{"/globalchat <pid> on/off", "Включить/отключить глобальный чат для игрока"},
		{"/globalchat on/off", "Включить/отключить глобальный чат для всех"},
		{"/mute <pid> on/off", "Включить/отключить мут для игрока"}
	}
}
       
data["help ordinator"] = {
	title = "Помощь ординатору",
	commands = {
		{"/kick <pid>", "Кикнуть игрока"},
		{"/ban <pid>", "Забанить игрока и все его ip"},
		{"/kill <pid>", "Убить игрока"},
		{"/unban <name>", "Разбанить игрока и все его ip"},
		{"/banlist", "Показать всех забаненых игроков"},
		{"/confiscate <pid>", "Открыть меню конфискации"},
		{"/teleport <pid>/all", "Телепортировать игрока к себе", "(/tp)"},
		{"/teleportto <pid>", "Телепортироваться к игроку", "(/tpto)"},
		{"/cells", "Показать лист всех загруженых ячеек"},
		{"/setattr <pid> <attribute> <value>", "Выставить атрибут игрока"},
		{"/setskill <pid> <skill> <value>", "Выставить скилл игрока"},
		{"/getkills <pid>", "Вывести список всех убитых игроком NPC"},
		{"/setkill <pid> <count> <refid>", "Выставить убийство NPC игроком в значение. 0 для удаления"},
		{"/setquestindex <pid> <index> <questid>", "Выставить индекс квеста"},
		{"/deletequest <pid> <questid>", "Удалить квест"},
		{"/getfactionexp <pid>", "Получить список изгнаний игрока из фракций"},
		{"/setfactionexp <pid> <faction> true/false", "Изгнать или восстановить игрока во фракции"}
	}
}

data["help ordinator page 2"] = {
	title = "Помощь ординатору, страница 2",
	commands = {}
}
         
data["help admin page 1"] = {
	title = tr("Admin commands ") .. tr(" page 1"),
	commands = {
		{"/addordinator <pid>", "Повысить игрока до ординатора"},
		{"/removeordiantor <pid>", "Понизить ординатора"},
		{"/addmoderator <pid>", "Повысить игрока до модератора"},
		{"/removemoderator <pid>", "Понизить модератора"},
		{"/setdifficulty <pid> <value>/default", "Установить сложность для игрока"},
		{"/setconsole <pid> on/off/default", "Включить/выключить консоль для игрока"},
		{"/setbedrest <pid> on/off/default", "Включить/выключить отдых в кровати для игрока"},
		{"/setwildrest <pid> on/off/default", "Включить/выключить отдых на природе для игрока"},
		{"/setwait <pid> on/off/default", "Включить выключить ожидание для игрока"},
		{"/placeat <pid> <refId>", "Поместить предмет в локацию игрока"},
		{"/spawnat <pid> <refId>", "Поместить существо или NPC в локацию игрока"},
		{"/disguise <pid> <refId>", "Сделать игрока определённым сущством или удалить его используя refid"},
		{"/usecreaturename <pid> on/off", "Установить имя существа, которым стал игрок в следствии предыдущей комманды"}
	}
}

data["help admin page 2"] = {
	title = tr("Admin commands ") .. tr(" page 2"),
	commands = {
		{"/setscale <pid> <value>", "Установить размер игрока"},
		{"/setloglevel <pid> <value>/default", "Установить лог-левел для игрока"},
		{"/setphysicsfps <pid> <value>/default", "Установить фреймрейт физики для игрока"},
		{"/setcollision <category> on/off", "Установить коллизии для категории объектов (PLAYER, ACTOR or PLACED_OBJECT)"},
		{"/overridecollision <refId> on/off", "Установить коллизии для refid до рестарта сервера"},
		{"/sethour <value>", "Установить текущее время"},
		{"/setday <value>", "Установить текущий день"},
		{"/setmonth <value>", "Установить текущий месяц"},
		{"/settimescale <value>", "Установить течение времени (30 по умолчанию -- 120 реальных секунд)"},
		{"/setauthority <pid> <cell>", "Сделать игрока владельцем ячейки", "(/setauth)"}
	}
}

local function CreateMenu(pid, id)
	local menuId = config.customMenuIds.helpMenu
	local message = ""
	
	if data[id].message ~= nil then
		message = message .. data[id].message .. "\n"
		message = message .. "\n\n"
	end
	
	for i, commands in pairs(data[id].commands) do
		message = message .. color.Yellow .. commands[1] .. "\n"
		message = message .. color.White .. commands[2]
		
		if commands[3] ~= nil then
			message = message .. " ".. color.Yellow .. commands[3]
		end
		
		message = message .. "\n"
	end
	
	mpWidgets.New(menuId, color.White .. data[id].title .. ":\n" .. message)
	
	for dest, button in orderedPairs(databuttons) do
		if Players[pid].data.settings.staffRank >= button.requireStuffRank and dest ~= id then
			mpWidgets.AddButton(menuId, databuttons[dest].title, function() CreateMenu(pid, dest) end)			
		end
	end
	
	mpWidgets.AddButton(menuId, "Выйти", nil)
	mpWidgets.Show(pid, menuId, 1)
end

-- DONT do this local, it is now global function
function ShowMenu(pid)
	CreateMenu(pid, "help player")
end

for key, value in pairs(commandInterface.GetAllButtons()) do
	databuttons[key] = {
		requireStuffRank = value.requireStuffRank,
		title = value.shortTitle
	}
			
	data[key] = {
		title = value.longTitle,
		message = value.description,
		commands = {}
	}
end

for key, value in pairs(commandInterface.GetAllCommands()) do
	if data[value.menuSection] == nil then
		databuttons[value.menuSection] = {}
		databuttons[value.menuSection].title = value.menuSection
		databuttons[value.menuSection].requireStuffRank = 0
		data[value.menuSection] = {}
		data[value.menuSection].title = value.menuSection
		data[value.menuSection].commands = {}
	end
	
	local arguments = ""
	if value.arguments ~= nil then
		arguments = " " .. value.arguments
	end
	
	table.insert(data[value.menuSection].commands, {"/" .. value.key .. arguments, value.description})
end
