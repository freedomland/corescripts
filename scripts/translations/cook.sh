#!/bin/sh

FILES=$(ls *.tr)

echo "return {" >> main.lua

for i in $FILES; do
	while read LINE; do
		echo "["$(echo $LINE | awk -F'=' '{print $1}')"] = " $(echo $LINE | awk -F'=' '{print $2}')"," >> main.lua
	done < $i
done

sed -i '$ s/.$//' main.lua

echo "}" >> main.lua
