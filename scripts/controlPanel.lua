logicHandler = require("logicHandler")
tableHelper = require("tableHelper")

Methods = {}

function Methods.SetAuthority(pid, targetPid, cellDescription)
	if logicHandler.CheckPlayerValidity(pid, targetPid) then
		-- Get rid of quotation marks
		cellDescription = string.gsub(cellDescription, '"', '')

		if logicHandler.IsCellLoaded(cellDescription) == true then
			logicHandler.SetCellAuthority(targetPid, cellDescription)
		else
			Players[pid]:Message("// #ff0000" .. tr("Cell \"") .. cellDescription .. tr("\" isn't loaded!\n"))
		end
	end
end

function Methods.AddOrdinator(pid, targetPid)
	if logicHandler.CheckPlayerValidity(pid, targetPid) then
		local targetName = Players[targetPid].chat.accountName

		if Players[targetPid]:IsOrdinator() then
			Players[pid]:Message("// " .. targetName .. "#ff0000" .. tr(" is already an Ordinator.\n"))
		else
			Players[targetPid].data.settings.staffRank = 2
			Players[targetPid]:QuicksaveToDisk()
			local message = "// " .. targetName .. "#ff0000" .. tr(" was promoted to Ordinator!\n")
			tes3mp.SendMessage(pid, message, true)
		end
	end
end

function Methods.RemoveOrdinator(pid, targetPid)
	if logicHandler.CheckPlayerValidity(pid, targetPid) then
		local targetName = Players[targetPid].chat.accountName

		if Players[targetPid]:IsAdmin() then
			Players[pid]:Message("// #ff0000" .. tr("Cannot demote ") .. targetName .. "#ff0000" .. tr(" because they are a Administrator.\n"))
		elseif Players[targetPid]:IsOrdinator() then
			Players[targetPid].data.settings.staffRank = 1
			Players[targetPid]:QuicksaveToDisk()
			local message = "// " .. targetName .. "#ff0000" .. tr(" was demoted from Ordinator to Moderator!\n")
			tes3mp.SendMessage(pid, message, true)
		else
			Players[pid]:Message("// " .. targetName .. "#ff0000" .. tr(" is not an Ordinator.\n"))
		end
	end
end

function Methods.AddModerator(pid, targetPid)
	if logicHandler.CheckPlayerValidity(pid, targetPid) then
		local targetName = Players[targetPid].chat.accountName

		if Players[targetPid]:IsOrdinator() then
			Players[pid]:Message("// " .. targetName .. "#ff0000" .. tr(" is already an Ordinator.\n"))
		elseif Players[targetPid]:IsModerator() then
			Players[pid]:Message("// " .. targetName .. "#ff0000" .. tr(" is already a Moderator.\n"))
		else
			Players[targetPid].data.settings.staffRank = 1
			Players[targetPid]:QuicksaveToDisk()
			local message = "// " .. targetName .. "#ff0000" .. tr(" was promoted to Moderator!\n")
			tes3mp.SendMessage(pid, message, true)
		end
	end
end

function Methods.RemoveModerator(pid, targetPid)
	if logicHandler.CheckPlayerValidity(pid, targetPid) then
		local targetName = Players[targetPid].chat.accountName

		if Players[targetPid]:IsOrdinator() then		
			Players[pid]:Message("// #ff0000" .. tr("Cannot demote ") .. targetName .. "#ff0000" .. tr(" because they are an Admin.\n"))
		elseif Players[targetPid]:IsModerator() then
			Players[targetPid].data.settings.staffRank = 0
			Players[targetPid]:QuicksaveToDisk()
			local message = "// #ff0000" .. targetName .. "#ff0000" .. tr(" was demoted from Moderator!\n")
			tes3mp.SendMessage(pid, message, true)
		else
			Players[pid]:Message("// #ff0000" .. targetName .. "#ff0000" .. tr(" is not a Moderator.\n"))
		end
	end
end

function Methods.Ban(pid, targetPid)
	if pid == targetPid then
		Players[pid]:Message("// #ff0000Вы не можете забанить себя.\n")
		return
	end

	if Players[targetPid] ~= nil then
		local targetName = Players[targetPid].accountName
		logicHandler.BanPlayer(pid, targetName)
		Players[targetPid]:Message("// #ff0000Вы были забанены.\n")
		Players[targetPid]:Kick()
	else
		Players[pid]:Message("// #ff0000" .. tr("Invalid input for ban.\n"))
	end
end

function Methods.Unban(pid, targetName)
	if targetName ~= nil then
		logicHandler.UnbanPlayer(pid, targetName)
	else
		Players[pid]:Message("// #ff0000" .. tr("Invalid input for unban.\n"))
	end
end

function Methods.Banlist(pid)
	local message = ""
	
	if #banList == 0 then
		Players[pid]:Message("// #ff0000" .. tr("No player or ip adresses have been banned.\n"))
		return
	end
	
	for index, player in pairs(banList.playerNames) do
		message = message .. player .. "\n"
	end
	
	tes3mp.ListBox(pid, -1, "Забаненые игроки:", message)
end

function Methods.Kick(pid, targetPid)
	if logicHandler.CheckPlayerValidity(pid, targetPid) then
		if Players[targetPid]:IsAdmin() then
			Players[pid]:Message("// #ff0000" .. tr("You cannot kick an Admin from the server.\n"))
		elseif Players[targetPid]:IsModerator() and not admin then
			Players[pid]:Message("// #ff0000" .. tr("Only Ordinators and Admins can controll moderators on this server.\n"))
		else
			local message = "// " .. Players[targetPid].chat.accountName .. "#ff0000" .. tr(" was kicked from the server.\n")
			tes3mp.SendMessage(pid, message, true)
			Players[targetPid]:Kick()
		end
	end
end

function Methods.Teleport(pid, command)
	if command == nil then
		Players[pid]:Message("// #ff0000 " .. tr("You must specify player."))
		return
	end

	if command ~= "all" then
		logicHandler.TeleportToPlayer(pid, command, pid)
	else
		for iteratorPid, player in pairs(Players) do
			if iteratorPid ~= pid then
				if player:IsLoggedIn() then
					logicHandler.TeleportToPlayer(pid, iteratorPid, pid)
				end
			end
		end
	end
end

-- but why?!
function Methods.SetMomentum(pid, targetPid, values)
	if logicHandler.CheckPlayerValidity(pid, targetPid) then
		if values.X ~= nil and values.Y ~= nil and values.Z ~= nil then
			tes3mp.SetMomentum(targetPid, values.X, values.Y, values.Z)
			tes3mp.SendMomentum(targetPid)
		else
			Players[pid]:Message("// #ff0000" .. tr("Not a valid argument. Use") .. " /setmomentum <pid> <x> <y> <z>\n")
		end
	end
end

function Methods.SetDifficulty(pid, targetPid, difficulty)
	if logicHandler.CheckPlayerValidity(pid, targetPid) then
		if difficulty == "default" or tonumber(difficulty) ~= nil then
			Players[targetPid]:SetDifficulty(tonumber(difficulty))
			Players[targetPid]:LoadSettings()
			Players[pid]:Message("// #ff0000Difficulty for " .. Players[targetPid].chat.accountName .. "#ff0000" .. tr(" is now ") .. difficulty .. "\n")
		else
			Players[pid]:Message("// #ff0000" .. tr("Not a valid argument. Use") .. " /setdifficulty <pid> <value>\n")
		end
	end
end

function Methods.SetConsole(pid, targetPid, command)
	if logicHandler.CheckPlayerValidity(pid, targetPid) then
		local targetName = ""
		local state = ""

		if command == "on" then
			Players[targetPid]:SetConsoleAllowed(true)
			state = tr(" enabled.\n")
		elseif command == "off" then
			Players[targetPid]:SetConsoleAllowed(false)
			state = tr(" disabled.\n")
		elseif command == "default" then
			Players[targetPid]:SetConsoleAllowed("default")
			state = tr(" reset to default.\n")
		else
			Players[pid]:Message("// #ff0000" .. tr("Not a valid argument. Use") .. " /setconsole <pid> on/off/default\n")
			return
		end

		Players[targetPid]:LoadSettings()
		Players[pid]:Message("// #ff0000" .. tr("Console for ") .. Players[targetPid].name .. "#ff0000" .. state)
		if targetPid ~= pid then
			Players[targetPid]:Message("// #ff0000" .. tr("Console ") .. state)
		end
	end
end

function Methods.SetBedRest(pid, targetPid, command)	
	if logicHandler.CheckPlayerValidity(pid, targetPid) then
		local targetName = ""
		local state = ""

		if command == "on" then
			Players[targetPid]:SetBedRestAllowed(true)
			state = tr(" enabled.\n")
		elseif command == "off" then
			Players[targetPid]:SetBedRestAllowed(false)
			state = tr(" disabled.\n")
		elseif command == "default" then
			Players[targetPid]:SetBedRestAllowed("default")
			state = tr(" reset to default.\n")
		else
			Players[pid]:Message("// #ff0000" .. tr("Not a valid argument. Use") .. " /setbedrest <pid> on/off/default\n")
			return
		end

		Players[targetPid]:LoadSettings()
		Players[pid]:Message("// #ff0000" .. tr("Bed resting for ") .. Players[targetPid].chat.accountName .. "#ff0000" .. state)
		if targetPid ~= pid then
			Players[targetPid]:Message("// #ff0000" .. tr("Bed resting ") .. state)
		end
	end
end

function Methods.SetWildRest(pid, targetPid, command)	
	if logicHandler.CheckPlayerValidity(pid, targetPid) then
		local targetName = ""
		local state = ""

		if command == "on" then
			Players[targetPid]:SetWildernessRestAllowed(true)
			state = tr(" enabled.\n")
		elseif command == "off" then
			Players[targetPid]:SetWildernessRestAllowed(false)
			state = tr(" disabled.\n")
		elseif command == "default" then
			Players[targetPid]:SetWildernessRestAllowed("default")
			state = tr(" reset to default.\n")
		else
			Players[pid]:Message("// #ff0000" .. tr("Not a valid argument. Use") .. " /setwildernessrest <pid> on/off/default\n")
			return
		end

		Players[targetPid]:LoadSettings()
		Players[pid]:Message("// #ff0000" .. tr("Wilderness resting for ") .. Players[targetPid].chat.accountName .. "#ff0000" .. state)
		if targetPid ~= pid then
			Players[targetPid]:Message("// #ff0000" .. tr("Wilderness resting ") .. state)
		end
	end
end

function Methods.SetWait(pid, targetPid, command)	
	if logicHandler.CheckPlayerValidity(pid, targetPid) then
		local targetName = ""
		local state = ""

		if command == "on" then
			Players[targetPid]:SetWaitAllowed(true)
			state = tr(" enabled.\n")
		elseif command == "off" then
			Players[targetPid]:SetWaitAllowed(false)
			state = tr(" disabled.\n")
		elseif command == "default" then
			Players[targetPid]:SetWaitAllowed("default")
			state = tr(" reset to default.\n")
		else
			Players[pid]:Message("// #ff0000" .. tr("Not a valid argument. Use") .. " /setwait <pid> on/off/default\n")
			return
		end

		Players[targetPid]:LoadSettings()
		Players[pid]:Message("// #ff0000" .. tr("Waiting for ") .. Players[targetPid].name .. "#ff0000" .. state)
		if targetPid ~= pid then
			Players[targetPid]:Message("// #ff0000" .. tr("Waiting ") .. "#ff0000" .. state)
		end
	end
end

function Methods.SetPhysicsFPS(pid, targetPid, fps)
	if logicHandler.CheckPlayerValidity(pid, targetPid) then
		if physicsFramerate == "default" or tonumber(fps) ~= nil then
			Players[targetPid]:SetPhysicsFramerate(tonumber(fps))
			Players[targetPid]:LoadSettings()
			Players[pid]:Message("// #ff0000" .. tr("Physics framerate for ") .. Players[targetPid].chat.accountName .. "#ff0000"
			 .. tr(" is now ") .. fps .. "\n")
		else
		Players[pid]:Message("// #ff0000" .. tr("Not a valid argument. Use") .. " /setphysicsfps <pid> <value>\n")
		end
	end
end

function Methods.SetLogLevel(pid, targetPid, logLevel)
	if logicHandler.CheckPlayerValidity(pid, cmd[2]) then
		if logLevel == "default" or tonumber(logLevel) ~= nil then
			Players[targetPid]:SetEnforcedLogLevel(tonumber(logLevel))
			Players[targetPid]:LoadSettings()
			Players[pid]:Message("// #ff0000" .. tr("Enforced log level for ") .. Players[targetPid].chat.accountName
		    .. "#ff0000" .. tr(" is now ") .. logLevel .. "\n")
		else
			Players[pid]:Message("// #ff0000" .. tr("Not a valid argument. Use") .. " /setloglevel <pid> <value>\n")
		end
	end
end

function Methods.SetHour(pid, inputValue)
	if tonumber(inputValue) ~= nil then
		if inputValue == 24 then
			inputValue = 0
		end

		if inputValue >= 0 and inputValue < 24 then
			WorldInstance.data.time.hour = inputValue
			WorldInstance:QuicksaveToDisk()
			WorldInstance:LoadTime(pid, true)
			hourCounter = inputValue
		else
			Players[pid]:Message("// #ff0000" .. tr("There aren't that many hours in a day.\n"))
		end
	end
end

function Methods.SetDay(pid, inputValue)
	if tonumber(inputValue) ~= nil then
		local daysInMonth = WorldInstance.monthLengths[WorldInstance.data.time.month]

		if inputValue <= daysInMonth then
			WorldInstance.data.time.day = inputValue
			WorldInstance:QuicksaveToDisk()
			WorldInstance:LoadTime(pid, true)
		else
			Players[pid]:Message("// #ff0000" .. tr("There are only ") .. daysInMonth .. tr(" days in the current month.\n"))
		end
	end
end

function Methods.SetMonth(pid, inputValue)
	if tonumber(inputValue) ~= nil then
		WorldInstance.data.time.month = inputValue
		WorldInstance:QuicksaveToDisk()
		WorldInstance:LoadTime(pid, true)
	end
end

function Methods.SetTimeScale(pid, inputValue)
	if tonumber(inputValue) ~= nil then
		WorldInstance.data.time.timeScale = inputValue
		WorldInstance:QuicksaveToDisk()
		WorldInstance:LoadTime(pid, true)
		frametimeMultiplier = inputValue / WorldInstance.defaultTimeScale
	end
end

function Methods.SetCollition(pid, category, command)
	local collisionState

	if category ~= nil and command == "on" then
	    collisionState = true
	elseif category ~= nil and command == "off" then
	    collisionState = false
	else
	     Players[pid]:Message("// #ff0000" .. tr("Not a valid argument. Use") .. " /setcollision <category> on/off\n")
	     return false
	end

	local categoryInput = string.upper(category)
	local categoryValue = enumerations.objectCategories[categoryInput]

	if categoryValue == enumerations.objectCategories.PLAYER then
	    tes3mp.SetPlayerCollisionState(collisionState)
	elseif categoryValue == enumerations.objectCategories.ACTOR then
	    tes3mp.SetActorCollisionState(collisionState)
	elseif categoryValue == enumerations.objectCategories.PLACED_OBJECT then
	    tes3mp.SetPlacedObjectCollisionState(collisionState)

	    if command == "on" then
			tes3mp.UseActorCollisionForPlacedObjects(true)
	    elseif command == "off" then
			tes3mp.UseActorCollisionForPlacedObjects(false)
	    end
	else
	    Players[pid]:Message("// #ff0000" .. categoryInput .. tr(" is not a valid object category. Valid choices are ") ..
		tableHelper.concatenateTableIndexes(enumerations.objectCategories, ", ") .. "\n")
	    return false
	end

	tes3mp.SendWorldCollisionOverride(pid, true)
	Players[pid]:Message("// #ff0000" .. tr("Collision for ") .. categoryInput .. tr(" is now ") .. tr(command) ..
	    tr(" for all newly loaded cells.\n"))
end

function Methods.OverrideCollision(pid, refid, command)
	local collisionState

	if refId ~= nil and command == "on" then
	    collisionState = true
	elseif refId ~= nil and command == "off" then
	    collisionState = false
	else
	    Players[pid]:Message("// #ff0000" .. tr("Not a valid argument. Use") .. " /addcollision <refId> on/off\n")
	    return false
	end

	local message = "// #ff0000" .. tr("A collision-enabling override ")

	if tableHelper.containsValue(config.enforcedCollisionRefIds, refId) then
	    if collisionState then
		message = message .. tr("is already on")
	    else
		tableHelper.removeValue(config.enforcedCollisionRefIds, refId)
		message = message .. tr("is now") .. tr(" off")
	    end
	else
	    if collisionState then
		table.insert(config.enforcedCollisionRefIds, refId)
		message = message .. tr("is now") .. tr(" on")
	    else
		message = message .. tr("is already") .. tr(" off")
	    end
	end

	logicHandler.SendConfigCollisionOverrides(pid, true)
	Players[pid]:Message(message .. tr(" for ") .. refId .. tr(" in newly loaded cells\n"))
end

function Methods.PlaceAt(pid, targetPid, refId, command)
	if logicHandler.CheckPlayerValidity(pid, targetPid) then
		local packetType

		if command == "placeat" then
			packetType = "place"
		elseif command == "spawnat" then
			packetType = "spawn"
		end

		logicHandler.CreateObjectAtPlayer(targetPid, refId, packetType)
	end
end

function Methods.Confiscate(pid, targetPid)
	if logicHandler.CheckPlayerValidity(pid, targetPid) then
		if targetPid == pid then
			Players[pid]:Message("// #ff0000" .. tr("You can't confiscate from yourself!\n"))
		elseif Players[targetPid].data.customVariables.isConfiscationTarget then
			Players[pid]:Message("// #ff0000" .. tr("Someone is already confiscating from that player\n"))
		else
			Players[pid].confiscationTargetName = Players[targetPid].accountName
			Players[targetPid]:SetConfiscationState(true)
			tableHelper.cleanNils(Players[targetPid].data.inventory)
			guiHelper.ShowInventoryList(config.customMenuIds.confiscate, pid, targetPid)
		end
	end
end

function Methods.GetKills(pid, targetPid)
	if logicHandler.CheckPlayerValidity(pid, targetPid) then
		local message = ""
	
		for i, t in pairs(Players[targetPid].data.kills) do
			message = message .. i .. " (" .. t .. ")" .. "\n"
		end

		tes3mp.ListBox(pid, -1, "Список убитых NPC:", message)
	end
end

function Methods.SetKill(pid, targetPid, count, refId)
	if logicHandler.CheckPlayerValidity(pid, targetPid) then
		if count == nil or refId == nil then
			Players[pid]:Message("// #ff0000Неверный ввод, используйте /setkill <targetPid> <count> <refId>" .. "\n")
			return
		end
		
		if Players[targetPid].data.kills[refId] == nil then
			Players[pid]:Message("// #ff0000 " .. Players[pid].accountName .. " не убивал " .. refId .. "\n")
			return
		end
		
		if count == 0 then
			Players[targetPid].data.kills[refId] = nil
			Players[pid]:Message("// #ff0000У игрока " .. Players[pid].accountName .. " было удалено убийство " .. refId .. "\n")
		else
			Players[targetPid].data.kills[refId] = count
			Players[pid]:Message("// #ff0000Игроку " .. Players[pid].accountName .. " было добавлено убийство " .. refId .. " в колличестве " .. count .. "\n")
		end
	
		tes3mp.InitializeKillChanges(targetPid)
		tes3mp.AddKill(targetPid, refId, count)
		tes3mp.SendKillChanges(targetPid)
	end
end

function Methods.DeleteQuest(pid, targetPid, quest)
	if logicHandler.CheckPlayerValidity(pid, targetPid) then
		if quest == nil then
			Players[pid]:Message("// #ff0000Неверный ввод, используйте /deletequest <targetPid> <questid>\n")
			return
		end

		local found = false

		for index, journalItem in pairs(Players[targetPid].data.journal) do
			if journalItem.quest == quest then
				Players[targetPid].data.journal[index] = nil
				found = true
			end
		end
		
		if found then
			logicHandler.RunConsoleCommandOnPlayer(targetPid, "setjournalindex \"" .. quest .. "\" " .. 0, false)
			Players[pid]:Message("// #ff0000Квест " .. quest .. " удалён для " .. Players[targetPid].accountName .. "\n")
		else
			Players[pid]:Message("// #ff0000Квест " .. quest .. " не найден у " .. Players[targetPid].accountName .. "\n")
		end
	end
end

function Methods.SetQuestIndex(pid, targetPid, questindex, quest)
	if logicHandler.CheckPlayerValidity(pid, targetPid) then
		if questindex == nil or quest == nil then
			Players[pid]:Message("// #ff0000Неверный ввод, используйте /setquestindex <targetPid> <index> <questid>" .. "\n")
			return
		end
		
		local found = false
		local tmp = {}
		local neededIndex = 0
		local questType = 0

		for index, journalItem in pairs(Players[targetPid].data.journal) do
			if journalItem.quest == quest then
				local q = {itemIndex = index, questindex = journalItem.index}
				tmp[#tmp+1] = q
				neededIndex = index
				questType = journalItem.type
				found = true
			end
		end
		
		if not found then
			Players[pid]:Message("// #ff0000Квест " .. quest .. " не найден у " .. Players[targetPid].accountName .. ", добавляю новый...\n")
			
			local journalItem = {
				type = questType,
				index = questindex,
				quest = quest,
				timestamp = {
					daysPassed = WorldInstance.data.time.daysPassed,
					month = WorldInstance.data.time.month,
					day = WorldInstance.data.time.day
				}
			}
				
			table.insert(Players[targetPid].data.journal, journalItem)
			logicHandler.RunConsoleCommandOnPlayer(targetPid, "journal \"" .. quest .. "\" " .. questindex, false)
			
			return
		end
				
		if #tmp > 1 then
			for i, t in pairs(tmp) do
				if t.questindex > questindex then
					Players[targetPid].data.journal[t.itemIndex] = nil
					tmp[i] = nil
				end
			end
			
			if #tmp == 0 then
				local journalItem = {
					type = questType,
					index = questindex,
					quest = quest,
					timestamp = {
						daysPassed = WorldInstance.data.time.daysPassed,
						month = WorldInstance.data.time.month,
						day = WorldInstance.data.time.day
					}
				}
				
				table.insert(Players[targetPid].data.journal, journalItem)
			end
		else
			Players[targetPid].data.journal[neededIndex].index = questindex
		end
		
		logicHandler.RunConsoleCommandOnPlayer(targetPid, "setjournalindex \"" .. quest .. "\" " .. questindex, false)
		Players[pid]:Message("// #ff0000Квест " .. quest .. " установлен в индекс " .. questindex .. " для " .. Players[targetPid].accountName .. "\n")
	end
end

function Methods.GetFactionExpulsion(pid, targetPid)
	if logicHandler.CheckPlayerValidity(pid, targetPid) then
		local message = ""
	
		for i, t in pairs(Players[targetPid].data.factionExpulsion) do
			if t then
				message = message .. i .. " (исключён)" .. "\n"
			else
				message = message .. i .. "\n"
			end
		end

		tes3mp.ListBox(pid, -1, "Список фракций игрока" .. Players[targetPid].accountName .. ":", message)
	end
end

function Methods.SetFactionExpulsion(pid, targetPid, faction, state)
	if logicHandler.CheckPlayerValidity(pid, targetPid) then
		if faction == nil or state == nil then
			Players[pid]:Message("// #ff0000Неверный ввод, используйте /setfactionexpulsion <targetPid> <faction> true/false\n")
			return
		end
	
		if state == "true" then
			state = true
		elseif state == "false" then
			state = false
		end
	
		tes3mp.ClearFactionChanges(pid)
		tes3mp.SetFactionChangesAction(pid, enumerations.faction.EXPULSION)

		tes3mp.SetFactionId(faction)
		tes3mp.SetFactionExpulsionState(state)
		tes3mp.AddFaction(pid)

		tes3mp.SendFactionChanges(pid)
	end
end

function Methods.SetAllowedKillPlayers(pid, targetPid, state)
	if logicHandler.CheckPlayerValidity(pid, targetPid) then
		if state == nil then
			Players[pid]:Message("// #ff0000Неверный ввод, используйте /allowedkillplayers <targetPid> true/false\n")
			return
		end
	
		if state == "true" then
			state = true
		elseif state == "false" then
			state = false
		end
	
		Players[targetPid].data.settings.allowedKillPlayers = state
		Players[targetPid]:QuicksaveToDisk()
	end
end

function Methods.KillPlayer(pid, targetPid)
	tes3mp.SetHealthCurrent(targetPid, 0)
	Players[pid]:SendMessage("// #ff0000 " .. Players[targetPid].accountName .. " был убит.")
end

return Methods
