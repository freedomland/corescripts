Methods = {}

local commands = {}
local buttons = {}

function Methods.Validate(cmd)
	if cmd[2] ~= nil and commands[cmd[1] .. " " .. cmd[2]] ~= nil then
		return true
	end

	if commands[cmd[1]] ~= nil then
		return true
	else
		return false
	end
end

function Methods.CommandCallback(pid, cmd, commandName)
	if cmd[2] ~= nil and commands[cmd[1] .. " " .. cmd[2]] ~= nil then
		commandName = cmd[1] .. " " .. cmd[2]
	end

	if Players[pid].data.settings.staffRank >= commands[commandName].stuffRankRequire then
		commands[commandName].func(pid, cmd)
	else
		Players[pid]:Message("// #ff0000" .. tr("Not a valid command. Type /help for more info.\n"))
	end
end

function Methods.GetAllCommands()
	local ret = {}
	
	for key, value in pairs(commands) do
		ret[#ret+1] = {key = key, description = value.description, menuSection = value.menuSection, arguments = value.arguments}
	end
	
	return ret
end

function Methods.GetAllButtons()
	return buttons
end

function Methods.AddCommand(commandName, options, func)
	if options.args ~= nil then
		commandName = commandName .. " " .. options.args
	end
	
	if commands[commandName] == nil then
		commands[commandName] = {}
	end
	
	commands[commandName].func = func
	commands[commandName].arguments = options.arguments
	commands[commandName].description = options.description
	commands[commandName].menuSection = options.menuSection
	commands[commandName].stuffRankRequire = options.stuffRankRequire
end

function Methods.AddButton(buttonName, shortTitle, longTitle, requireStuffRank, description)
	if buttons[buttonName] == nil then
		buttons[buttonName] = {}
		buttons[buttonName].shortTitle = shortTitle
		buttons[buttonName].longTitle = longTitle
		buttons[buttonName].requireStuffRank = requireStuffRank
		buttons[buttonName].description = description
	end
end

return Methods
