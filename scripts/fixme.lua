logicHandler = require("logicHandler")
config = require("config")

Methods = {}

local timer

function fixmetimer(pid)
	logicHandler.RunConsoleCommandOnPlayer(pid, "fixme", false)
	tes3mp.StopTimer(timer)
	timer = nil
end

function Methods.FixMe(pid)
	local currentTime = os.time()
	local cell = tes3mp.GetCell(pid)

	if Players[pid].data.customVariables.lastFixMe == nil or
		currentTime >= Players[pid].data.customVariables.lastFixMe + config.fixmeInterval then
		
		-- isExterior doesn't work and I dont know why
		local currentCell = cell:split(",")
		
		if tonumber(currentCell[1]) ~= nil then
			local Z = Players[pid].data.location.posZ
			logicHandler.RunConsoleCommandOnPlayer(pid, "setpos Z " .. config.fixmeStrenght, false)
			timer = tes3mp.CreateTimerEx("fixmetimer", time.seconds(1), "i", pid)
			tes3mp.StartTimer(timer)
		else
			logicHandler.RunConsoleCommandOnPlayer(pid, "coc \"" .. cell .. "\"", false)
		end
		
		Players[pid].data.customVariables.lastFixMe = currentTime
		tes3mp.MessageBox(pid, -1, tr("Looks like you're get of this place"))
	else
		local remainingSeconds = Players[pid].data.customVariables.lastFixMe + config.fixmeInterval - currentTime
		local message = tr("You can't use /fixme for more ")

		if remainingSeconds > 1 then
			message = message .. remainingSeconds .. tr(" seconds")
		else
			message = message .. tr("one second")
		end

		message = message .. "\n"
		tes3mp.MessageBox(pid, -1, message)
	end
end

return Methods
