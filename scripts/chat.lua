require("translate")
config = require("config")
color = require("color")
logicHandler = require("logicHandler")
mpWidgets = require("mpWidgets")

local channels = {
	["local"] = {
		color = "#FFFFFF",
		prefix = "",
		title = "Локальный",
		display_time = true,
		local_chat = true
	},
	
	["global"] = {
		color = "#FFFFFF",
		prefix = "#6CC700всем#FFFFFF",
		title = "Глобальный",
		display_time = true,
		local_chat = false
	},
	
	["bot"] = {
		color = "#FF00BA",
		prefix = "",
		title = "Боты",
		display_time = true,
		local_chat = false
	},
	
	["server"] = {
		color = "#299600",
		prefix = ">",
		display_time = true,
		local_chat = false
	},
	
	["system_error"] = {
		color = "#FF0000",
		prefix = "//",
		local_chat = false
	},
	
	["system_info"] = {
		color = "#FFFFFF",
		prefix = "//",
		local_chat = false
	}
}

local chatHelper = {}
chatHelper.globalChatEnabled = true

-- [ utils ] --
local function GetCurrentTime()
	local t = os.date("%H:%M", os.time())
	local str = "[".. t .."] "
	return str
end

local function IsPlayerMuted(pid)
	if Players[pid].data.chat.muted == true then
		tes3mp.SendMessage(pid, "// #ff0000" .. tr("You was permanently muted.\n"), false)
		return true
	end
	
	return false
end

function GetPlayerByName(targetName)
	for iteratorPid, player in pairs(Players) do
		if string.lower(targetName) == string.lower(player.accountName) then
			return player.pid
		end
	end
	
	return nil
end

function chatHelper.ChatLog(pid, cmd)
	local name = tes3mp.GetName(pid)
	local ip = Players[pid].data.ipAddresses
	
	if IsPlayerMuted(pid) then
		name = name .. " [Muted]"
	end

	local file = io.open(tes3mp.GetDataPath() .. "/chat/logs.txt", "a")
	file:write(os.date("[ %X | %x | ", os.time()) .. ip[#ip] .. "  ] " .. name .. ": " .. cmd .. "\n")
	file:close()
end

-- [ admin ] --
function chatHelper.GlobalChatAdmin(pid, command)
	local message = "// #ff0000" .. tr("Global chat was ")
	if command == "on" then
		chatHelper.globalChatEnabled = true
		tes3mp.SendMessage(pid, message .. tr("enabled.") .. "\n", true)
	elseif command == "off" then
		chatHelper.globalChatEnabled = false
		tes3mp.SendMessage(pid, message .. tr("disabled.") .. "\n", true)
	else
		chatHelper.globalChatEnabled = true
		tes3mp.SendMessage(pid, message .. tr("enabled.") .. "\n", true)
	end
end

function chatHelper.Mute(pid, targetPid)
	if pid == targetPid then
		Players[pid]:Message("// #ff0000" .. tr("You can't mute yourself") .. "\n")
		return
	end
	
	if Players[targetPid]:IsAdmin() or Players[targetPid]:IsOrdinator() then
		Players[pid]:Message("// #ff0000" .. tr("Admins and Ordinators can't be muted") .. "\n")
		return
	end
	
	if not Players[targetPid]:IsLoggedIn() then
		Players[pid]:Message("// #ff0000" .. tr("Player doesn't exist or not login in") .. "\n")
		return
	end
	
	local muted = Players[targetPid].data.chat.muted
	
	if muted == false then
		tes3mp.SendMessage(pid, Players[targetPid].accountName .. "#8CC78A" .. tr(" was muted!\n"), true)
		Players[targetPid].data.chat.muted = true
		Players[targetPid]:Save()
	elseif muted == true then
		tes3mp.SendMessage(pid, Players[targetPid].accountName .. "#8CC78A" .. tr(" was unmuted!\n"), true)
		Players[targetPid].data.chat.muted = false
		Players[targetPid]:Save()
	end
end

-- [ main ] --
-- used also in all rp commands
function SendLocalMessage(pid, cmd, names)
	if IsPlayerMuted(pid) then
		return
	end

	local cellDescription = Players[pid].data.location.cell

	if logicHandler.IsCellLoaded(cellDescription) == true then
		for index, visitorPid in pairs(LoadedCells[cellDescription].visitors) do
			if not Players[visitorPid].chat.localHidden then
				if names then
					local message = GetCurrentTime() .. Players[pid].chat.accountName .. ": "
					message = message .. cmd .. "\n"
					tes3mp.SendMessage(visitorPid, message, false)
				else
					tes3mp.SendMessage(visitorPid, GetCurrentTime() .. cmd .. "\n", false)
				end
			end
		end
	end
end

function SendGlobalMessage(pid, cmd)
	if chatHelper.globalChatEnabled == false then
		Players[pid]:Message("// #ff0000" .. tr("Global chat was restricted by administrator.\n"))
		return
	end
	
	if IsPlayerMuted(pid) then
		return
	end
	
	local message = GetCurrentTime() .. "#6CC700[GC] " .. Players[pid].chat.accountName .. ": " .. cmd .. "\n"

	for i, t in pairs(Players) do
		if not t.chat.globalHidden then
			tes3mp.SendMessage(t.pid, message, false)
		end
	end
end

function chatHelper.SendGlobalMessage(pid, cmd)
	SendGlobalMessage(pid, cmd)
end

function chatHelper.SendLocalMessage(pid, cmd)
	SendLocalMessage(pid, cmd, true)
end

function chatHelper.OnPlayerConnect(pid)
	for key, channel in pairs(channels) do
		if channel.title ~= nil then
			Players[pid].chat.channels[key] = true
		end
	end			
end

-- [ GUI ] --
function chatHelper.ChatOptionsDialog(pid)
	local menuId = config.customMenuIds.chatOptions
	local count = 0
	local keycount = 1
	
	mpWidgets.New(menuId, "Вы можете включить или отключить некоторые каналы чата. По умолчанию все каналы включены. Отключение каналов производится на сессию. Минус перед каналом означает что он включен, плюс, что включен.")
	
	for key, channel in pairs(channels) do
		if channel.title ~= nil then
			mpWidgets.AddCheckBox(pid, menuId, count, channel.title, Players[pid].chat.channels[key])
			count = count + 1
		end
	end
	
	mpWidgets.OnDoneClicked(pid, menuId, function(tab) 
		for key, channel in pairs(Players[pid].chat.channels) do
			Players[pid].chat.channels[key] = tab[keycount]
			keycount = keycount + 1
		end
	end)
	
	mpWidgets.Show(pid, menuId, 4)
end

function chatHelper.OnPlayerCommand(pid, cmd)
	local hidden = false
	local msg = tr("hidden")
	local msg2 = ""

	if cmd == "global" then					
		Players[pid].chat.globalHidden = not Players[pid].chat.globalHidden
		hidden = Players[pid].chat.globalHidden
	elseif cmd == "local" then
		Players[pid].chat.localHidden = not Players[pid].chat.localHidden
		hidden = Players[pid].chat.globalHidden
	end
			
	if not hidden then
		msg = tr("unhidden")
	end
			
	if cmd == "global" then
		msg2 = tr("Global")
	else
		msg2 = tr("Local")
	end
			
	Players[pid]:Message(msg2 .. tr(" was") .. msg .. tr(" to you.\n"))
end

-- [ rp ] --
function chatHelper.PlayerMeCommand(pid, cmd)
	if not IsPlayerMuted(pid) then
		if cmd == nil then
			Players[pid]:Message("// #ff0000" .. tr("You cannot send a blank message.\n"))
		else
			local message = config.chatColors.action .. "* " .. Players[pid].chat.accountName .. " " .. config.chatColors.action .. cmd .. " *" .. color.White
			SendLocalMessage(pid, message, false)
		end
	end
end

function chatHelper.PlayerDoCommand(pid, cmd)
	if not IsPlayerMuted(pid) then

		if cmd == nil then
			Players[pid]:Message("// #8CC78A" .. tr("You cannot send a blank message.\n"))
		else
			local message = config.chatColors.action .. "* " .. cmd .. " " .. color.White .. "| " .. Players[pid].chat.accountName
			SendLocalMessage(pid, message, false)
		end
	end
end

function chatHelper.PlayerTodoCommand(pid, cmd)
	if not IsPlayerMuted(pid) then
		local tmp = cmd:split("*")
				
		if tmp[1] == nil or tmp[2] == nil then
			Players[pid]:Message("// #ff0000" .. tr("Invalid arguments in command /todo. Type /help to get more help about this command.\n"))
		else
			local message = "* " .. tmp[1] .. "* " ..  config.chatColors.action .. tr("Said ") .. Players[pid].chat.accountName .. config.chatColors.action .. ", " .. tmp[2] .. "."
			SendLocalMessage(pid, message, false)
		end
	end
end

function chatHelper.PlayerTryCommand(pid, cmd)
	if not IsPlayerMuted(pid) then
		if cmd == nil then
			Players[pid]:Message("// #ff0000" .. tr("You cannot send a blank message.\n"))
		else
			local message = config.chatColors.action .. "* " .. Players[pid].chat.accountName .. " " .. config.chatColors.action .. cmd .. " *"
			math.randomseed( tonumber(tostring(os.time()):reverse():sub(1,2)) )
			if math.random(1,2) == 1 then
				message = message .. " #36FF00" .. tr("(successfully)") .. "#FFFFFF"
			else
				message = message .. " #FF1100" .. tr("(unsuccessfully)") .. "#FFFFFF"
			end
				
			SendLocalMessage(pid, message, false)
		end
	end
end

-- [ private ] --
function chatHelper.PlayerPrivateMessage(pid, targetPid, message)
	if not IsPlayerMuted(pid) then
		-- see PlayerPrivateMessageToName
		if tonumber(targetPid) == nil then
			Players[pid]:Message("// #ff0000" .. tr("Invalid player or player nick used without double quotes.\n"))
			return
		end
	
		if pid == tonumber(targetPid) then
			Players[pid]:Message("// #ff0000" .. tr("You can't message yourself.\n"))
		elseif message == nil then
			Players[pid]:Message("// #ff0000" .. tr("You cannot send a blank message.\n"))
		elseif logicHandler.CheckPlayerValidity(pid, targetPid) then
			local targetPid = tonumber(targetPid)
			local targetName = Players[targetPid].name
			Players[pid]:Message(config.chatColors.action .. tr("You whisper to ") .. Players[targetPid].chat.accountName .. ":#FFFFFF " .. message .. "\n")
			Players[targetPid]:Message(Players[pid].chat.accountName .. config.chatColors.action .. tr(" whispered to you:") .. " #FFFFFF" .. message .. "\n")
		end
	end
end

function chatHelper.PlayerPrivateMessageToName(pid, cmd)
	if not IsPlayerMuted(pid) then	
		cmd = cmd:split(" ")
		local targetPid = ""
		local message = ""
		local index = 1
		
		-- concentrate all name strings, last doublequote is end of name
		for i, t in pairs(cmd) do
			local tmp = string.sub(t, string.len(t))
			-- last doublequote
			if tmp ~= nil and tmp == "\"" then	
				targetPid = targetPid .. " " .. t
				break
			end
	
			targetPid = targetPid .. " " .. t
			
			index = index + 1 -- store index of end of name
		end
	
		-- remove first space and doublequote
		targetPid = string.gsub(targetPid, "%W\"", "")
		-- remove last space and doublequote
		targetPid = string.gsub(targetPid, "\"", "")
		targetPid = GetPlayerByName(targetPid)
		
		if targetPid == nil then
			Players[pid]:Message("#ff0000" .. tr("Player is not login in or doesn't exist.\n"))
			return
		end
		
		-- retrive message
		for i = index+1, #cmd do
			message = message .. " " .. cmd[i]
		end
	
		if pid == tonumber(targetPid) then
			Players[pid]:Message("#ff0000" .. tr("You can't message yourself.\n"))
		elseif message == nil then
			Players[pid]:Message("#ff0000" .. tr("You cannot send a blank message.\n"))
		elseif logicHandler.CheckPlayerValidity(pid, targetPid) then
			local targetPid = tonumber(targetPid)
			local targetName = Players[targetPid].name
			Players[pid]:Message(config.chatColors.action .. tr("You whisper to ") .. Players[targetPid].chat.accountName .. ":#FFFFFF" .. message .. "\n")
			Players[targetPid]:Message(Players[pid].chat.accountName .. config.chatColors.action .. tr(" whispered to you:") .. "#FFFFFF" .. message .. "\n")
		end
	end
end

function chatHelper.SendMessageInChannel(pid, message, channel, toAllPlayers)
	local cp = channels[channel].prefix
	local cc = channels[channel].color
	local lc = channels[channel].local_chat

	if toAllPlayers == nil or toAllPlayers == false then
		if Players[pid].chat.channels[channel] then
			Players[pid]:Message(cp .. cc .. message)
		end
	else
		if lc == true then
			SendLocalMessage(pid, message)
		else
			SendGlobalMessage(pid, message)
		end
	end
end

return chatHelper
