require("translate")

config = {}

-- The game mode displayed for this server in the server browser
config.gameMode = "Default"

-- Time to login, in seconds
config.loginTime = 60

-- The difficulty level used by default
-- Note: In OpenMW, the difficulty slider goes between -100 and 100, with 0 as the default,
--       though you can use any integer value here
config.difficulty = 0

-- The world time used for a newly created world
config.defaultTimeTable = { year = 427, month = 7, day = 16, hour = 9,
    daysPassed = 1, timeScale = 30 }

-- The chat window instructions that show up when players join the server
config.chatWindowInstructions = color.White .. tr("Use ") .. color.Yellow .. "Y" .. color.White .. tr(" to chat. ") ..
    tr("Type in ") .. color.Yellow .. "/help" .. color.White .. tr(" to see the commands available to you. Use ") .. 
    color.Yellow .. "F2" .. color.White .. tr(" to hide the chat window. Use ") .. color.Yellow .. tr("/a <text>") .. color.White .. tr(" to write to global chat.\n")
    
config.chatColors = { action = "#927eaf" }

-- Whether the world time should continue passing when there are no players on the server
config.passTimeWhenEmpty = false

-- Whether players should be allowed to use the ingame tilde (~) console by default
config.allowConsole = false

-- Whether players should be allowed to rest in bed by default
config.allowBedRest = true

-- Whether players should be allowed to rest in the wilderness by default
config.allowWildernessRest = false

-- Whether players should be allowed to wait by default
config.allowWait = false

-- The cell that newly created players are teleported to
config.defaultSpawnCell = "Seyda Neen"

-- The X, Y and Z position that newly created players are teleported to
config.defaultSpawnPos = {-10798.414062, -72155.320312, 153.000000}

-- The X and Z rotation that newly created players are assigned
config.defaultSpawnRot = {0.447623, 0.000000}

-- The cell that players respawn in, unless overridden below by other respawn options
config.defaultRespawnCell = "Balmora, Temple"

-- The X, Y and Z position that players respawn in
config.defaultRespawnPos = {4700.5673828125, 3874.7416992188, 14758.990234375}

-- The X and Z rotation that respawned players are assigned
config.defaultRespawnRot = {0.25314688682556, 1.570611000061}

-- Whether the default respawn location should be ignored in favor of respawning the
-- player at the nearest Imperial shrine
config.respawnAtImperialShrine = false

-- Whether the default respawn location should be ignored in favor of respawning the
-- player at the nearest Tribunal temple
-- Note: When both this and the Imperial shrine option are enabled, there is a 50%
--       chance of the player being respawned at either
config.respawnAtTribunalTemple = true

-- The maximum value that any attribute except Speed is allowed to have
config.maxAttributeValue = 200

-- The maximum value that any skill except Acrobatics is allowed to have
config.maxSkillValue = 200

-- The maximum value that Speed is allowed to have
-- Note: Speed is given special treatment because of the Boots of Blinding Speed
config.maxSpeedValue = 365
config.maxStrengthValue = 500
config.maxAgilityValue = 500
config.maxPersonalityValue = 350
config.maxSpeedValue = 350
config.maxLuckValue = 200
config.maxEnduranceValue = 350
config.maxIntelligenceValue = 300
config.maxWillpowerValue = 350

-- The maximum value that Acrobatics is allowed to have
-- Note: Acrobatics is given special treatment because of the Scroll of Icarian Flight
config.maxAcrobaticsValue = 1200
config.maxBlockValue = 350
config.maxAlchemyValue = 350
config.maxRestorationValue = 350
config.maxConjurationValue = 350
config.maxMarksmanValue = 350
config.maxMercantileValue = 350
config.maxAlterationValue = 350
config.maxHeavyarmorValue = 350
config.maxMediumarmorValue = 350
config.maxShortbladeValue = 350
config.maxSneakValue = 350
config.maxLightarmorValue = 350
config.maxLongbladeValue = 350
config.maxArmorerValue = 350
config.maxSpeechcraftValue = 350
config.maxAxeValue = 350
config.maxSecurityValue = 350
config.maxEnchantValue = 350
config.maxDestructionValue = 350
config.maxAthleticsValue = 350
config.maxIllusionValue = 350
config.maxMysticismValue = 350
config.maxSpearValue = 350
config.maxHandtohandValue = 350
config.maxBluntweaponValue = 350
config.maxUnarmoredValue = 350

-- The refIds of items that players are not allowed to equip for balancing reasons
config.bannedEquipmentItems = { }

-- Whether players should respawn when dying
config.playersRespawn = true

-- Time to stay dead before being respawned, in seconds
config.deathTime = 5

-- The number of days spent in jail as a penalty for dying, when respawning
config.deathPenaltyJailDays = 5

-- Whether players' bounties are reset to 0 after dying
config.bountyResetOnDeath = true

-- Whether players spend time in jail proportional to their bounty after dying
-- Note: Requires bountyResetOnDeath to be enabled
config.bountyDeathPenalty = true

-- Whether players should be allowed to use the /suicide command
config.allowSuicideCommand = true

-- Whether players should be allowed to use the /fixme command
config.allowFixme = true

-- How many seconds need to pass between uses of the /fixme command by a player
config.fixmeInterval = 30

-- New fixme using setpos Z with fixme instead of just fixme, witch doens't work right now in OpenMW
-- This setting control how hieght player can jump using fixme
config.fixmeStrenght = 1000

-- Maximum items count in stack, be careful! tes3mp only know int32, so you can't add more then 2,147,483,647
config.maximumStack = 147483646

-- The colors used for different ranks on the server
config.rankColors = { admin = color.Red, ordinator = color.Orange, moderator = color.SkyBlue, server = color.Green, bot = color.Violet }

-- Which numerical IDs should be used by custom menus implemented in the Lua scripts,
-- to prevent other menu inputs from being taken into account for them
config.customMenuIds = { helpMenu = 9001, confiscate = 9002, recordPrint = 9003, chatOptions = 9004, HSSell = 9005, HSMultsell = 9006, HSLock = 9007, HSGuestadd = 9008, HSGuestremove = 9009, HSRent = 9010, FLSTurt = 9011, FLSEquip = 9012, FLSKit = 9013, FLArenaReg = 9014, PlayerMenu = 9015, MainMenu = 9016, FLTeach = 9017, FLTeachWarn = 9018, FLTeachPrice = 9019, FLTeachConfirm = 9020, FLTeachCheck = 9021, FLShrine = 9022 }

-- What the difference in ping needs to be in favor of a new arrival to a cell or region
-- compared to that cell or region's current player authority for the new arrival to become
-- the authority there
-- Note: Setting this too low will lead to constant authority changes which cause more lag
config.pingDifferenceRequiredForAuthority = 40

-- The log level enforced on clients by default, determining how much debug information
-- is displayed in their debug window and logs
-- Note 1: Set this to -1 to allow clients to use whatever log level they have set in
--         their client settings
-- Note 2: If you set this to 0 or 1, clients will be able to read about the movements
--         and actions of other players that they would otherwise not know about,
--         while also incurring a framerate loss on highly populated servers
config.enforcedLogLevel = -1

-- The physics framerate used by default
-- Note: In OpenMW, the physics framerate is 60 by default, but TES3MP has slightly higher
--       system requirements that make a default of 30 more appropriate.
config.physicsFramerate = 30

-- Whether players should collide with other actors
config.enablePlayerCollision = true

-- Whether actors should collide with other actors
config.enableActorCollision = true

-- Whether placed objects should collide with actors
config.enablePlacedObjectCollision = false

-- Enforce collision for certain placed object refIds even when enablePlacedObjectCollision
-- is false
config.enforcedCollisionRefIds = {}

-- Whether placed object collision (when turned on) resembles actor collision, in that it
-- prevents players from standing on top of the placed objects without slipping
config.useActorCollisionForPlacedObjects = false

-- Prevent certain object refIds from being deleted as a result of player-sent packets
config.disallowedDeleteRefIds = { "thief_ring", "artifact_bittercup_01", "redoran_master_helm", "dagger of judgement", "misc_skull_llevule", "misc_dwrv_ark_cube00", "bk_ajira2", "bk_ajira1", "demon longbow", "misc_dwrv_artifact40", "keening", "misc_dwrv_artifact10", "misc_dwrv_artifact20", "misc_dwrv_artifact30", "p_vintagecomberrybrandy1", "key_standard_01", "key_temple_01", "key_standard_01_pel_guard_tower", "key_kagouti_colony", "key_obscure_alit_warren", "key_fetid_dreugh_grotto", "key_redoran_basic", "key_chest_aryniorethi_01", "key_chest_coduscallonus_01", "key_chest_drinarvaryon_01", "key_standard_01_pel_fort_prison", "key_falas tomb keepers", "key_falas tomb keepers_2", "key_assarnud", "key_pellecia aurrus", "key_bivaleteneran_01", "key_hlormarenslaves_01", "key_chest_avonravel_01", "key_chest_brilnosullarys_01", "key_llethervari_01", "key_llethrimanor_01", "key_standard_darius_chest", "key_tukushapal_1", "key_sarethimanor_01", "key_tuvesobeleth_01", "key_malpenixblonia_01", "key_arobarmanor_01", "key_arobarmanorguard_01", "key_ciennesintieve_01", "key_dumbuk_strongbox", "key_gnisis_eggmine", "key_madach_room", "key_arvs-drelen_cell", "key_summoning_room", "key_standard_01_darvam hlaren", "key_standard_01_hassour zainsub", "key_hinnabi", "key_minabi", "key_hasphat_antabolis", "key_hasphat_antabolis2", "key_shushishi", "key_huleen's_hut", "key_assemanu_01", "key_assemanu_02", "key_nund", "key_divayth_fyr", "key_arrile", "key_ra'zhid", "key_ministry_cells", "key_ministry_sectors", "key_rothran", "key_aldsotha", "key_kogoruhn_sewer", "key_ministry_ext", "key_persius mercius", "key_impcomsecrdoor", "key_saryoni", "key_gen_tomb", "key_hodlismod", "key_falaanamo", "key_irgola", "key_savilecagekey", "key_dren_manor", "key_helvi", "key_dralor", "key_ivrosa", "key_dren_storage", "key_orvas_dren", "key_nelothtelnaga", "key_keelraniur", "key_nedhelas", "key_fedar", "key_sethan", "key_telbranoratower", "key_tel_aruhn_slave1", "key_savilecagekey02", "key_archcanon_private", "key_vivec_secret", "key_gro-bagrat", "key_shipwreck9-11", "key_varostorage", "key_dreynos", "key_ienasa","key_ulvil", "key_varoprivate", "key_mette", "key_itar", "key_anja", "key_cabin", "key_shilipuran", "key_assi", "key_kind", "key_galmis", "key_fals", "key_tureynul", "key_yagram", "key_vivec_arena_cell", "key_olms_storage", "key_vorarhelas", "key_tgbt", "key_neranomanor", "key_ald_redaynia", "key_berandas", "key_divayth00", "key_divayth01", "key_divayth02", "key_divayth03", "key_divayth04", "key_divayth05", "key_divayth06", "key_divayth07", "key_odros", "key_eldrar", "key_alvur", "key_morvaynmanor", "key_gshipwreck", "misc_uniq_egg_of_gold", "key_nelothtelnaga2", "key_nelothtelnaga3", "key_nelothtelnaga4", "key_adibael", "key_omani_01", "key_redoran_treasury", "key_ahnassi", "key_vivec_hlaalu_cell", "key_vivec_redoran_cell", "key_vivec_telvanni_cell", "key_slave_addamasartus", "key_balmorag_tong_01", "key_balmorag_tong_02", "misc_dwrv_artifact_ils", "key_aldruhn_underground", "key_desele", "key_suran_slave", "key_sn_warehouse", "key_menta_na", "key_marvani_tomb", "misc_com_bucket_boe_uni", "key_ebon_tomb", "key_draramu", "key_oritius", "key_saetring", "key_molagmarslaves_01", "key_kudanatslaves_01", "key_yakanalitslaves_01", "key_gatewayinnslaves_01", "key_sinsibadonslaves_01", "key_minabislaves_01", "key_abebaalslaves_01", "key_zainsipiluslaves_01", "key_drenplantationslaves_01", "key_aharunartusslaves_01", "key_telvosjailslaves_01", "key_sadrithmoraslaves_01", "key_shushanslaves_01", "key_rotheranslaves_01", "key_addamasartusslaves_01", "key_zebabislaves_01", "key_hinnabislaves_01", "key_telaruhnslaves_01", "key_calderaslaves_01", "key_vivectelvannislaves_01", "key_panatslaves_01", "key_saturanslaves_01", "key_shaadniusslaves_01", "key_suranslaves_01", "key_telbranoraslaves_01", "key_viveclizardheadslave_01", "key_habinbaesslaves_01", "key_assarnudslaves_01", "key_llethri", "key_shushishislaves", "key_shaadnius", "key_eldafire", "key_farusea_salas", "key_relien_rirne", "key_sirilonwe", "key_fg_nchur", "key_odibaal", "key_ashurninibi", "key_ashurninibi_lost", "key_ashalmawia_prisoncell", "key_drarayne_thelas", "key_dulnea_ralaal", "key_ralen_hlaalo", "key_nileno_dorvayn", "key_ibardad", "key_ibardad_tomb", "key_hanarai_assutlanipal", "key_sandas", "key_aurane1", "key_arenim", "key_odral_helvi", "key_assi_serimilk", "key_cell_buckmoth_01", "key_cell_ebonheart_01", "key_hlaalo_manor", "key_ashirbadon", "key_senim_tomb", "key_gimothran", "key_murudius_01", "key_punsabanit", "key_yinglingbasement", "key_elmussadamori", "key_volrina_01", "key_fqt", "key_skeleton", "key_hvaults1", "key_hvaults2", "key_indaren", "key_venim", "key_aralen", "key_sarys_chest", "key_tharys_chest", "key_thelas_chest", "key_othrelas_door", "key_arano_door", "key_arano_chest", "key_andrethi_chest", "key_heran", "key_lleran_tomb", "key_brinne_chest", "key_aran_tomb", "key_sandas_tomb", "key_sarano_tomb", "key_sarano_chest", "key_saren_chest", "key_saren_tomb", "key_vandus_tomb", "key_maren_tomb", "key_raviro_tomb", "key_andalen_tomb", "key_andalen_chest", "key_arenim_chest", "key_ravel_tomb", "key_ravel_chest", "key_savel_tomb", "key_indalen_tomb", "key_norvayn_tomb", "key_norvayn_chest", "key_aryon_chest", "key_fadathram_tomb", "key_helas_tomb", "key_thalas_tomb", "key_andas_tomb", "key_andules_chest", "key_gimothran_chest", "key_gimothran_tomb", "key_ienith_tomb", "key_ienith_chest", "key_thiralas_tomb", "key_baram_tomb", "key_dreloth_tomb", "key_omaren_chest", "key_sadryon_tomb", "key_verelnim_tomb", "key_falas_tomb", "key_falas_chest", "key_llervu", "key_rethandus_tomb", "key_rethandus_chest", "key_rothan_tomb", "key_dareleth_tomb", "key_salvel_tomb", "key_salvel_chest", "key_nerano_chest", "key_andavel_tomb", "key_dralas_tomb", "key_dralas_chest", "key_nelas_chest", "key_omalen_tomb", "key_orethi_tomb", "key_sarethi_tomb", "key_favel_chest", "key_senim_chest", "key_widow_vabdas", "key_galom_daeus", "key_ashmelech", "key_ashmelech_chest", "key_nchuleftingth_chest", "key_nchuleftingth", "key_mzahnch_chest", "key_aleft_chest", "key_mzanchend_chest", "key_arkngthunch_chest", "key_bthanchend_chest", "key_bthuand", "key_mzuleft", "key_nchardahrk", "key_nchardahrk_chest", "key_venimmanor", "key_rvaults1", "key_table_mudan00", "key_door_mudan00", "key_mudan_dragon", "key_odirniran", "key_dawnvault", "key_duskvault", "key_j'zhirr", "misc_dwarfbone_unique", "key_tvault", "key_armigers_stronghold", "key_thorek", "key_tv_ct", "key_private quarters", "key_caryarel", "key_dubdilla", "key_forge of rolamus", "lucky_coin", "key_wormlord_tomb", "key_mebastien", "key_miles", "key_palansour", "key_miun_gei", "key_brallion", "key_shashev", "key_camp", "key_dura_gra-bol", "key_caius_cosades", "key_rufinus_alleius", "key_jeanne", "key_bolayn", "key_gindrala", "misc_uni_pillow_unique", "misc_goblet_dagoth", "bk_eggoftime", "pyroil_tar_unique", "bk_irano_note" }

-- Prevent certain object refIds from being placed or spawned as a result of player-sent packets
config.disallowedCreateRefIds = {}

-- Prevent certain object refIds from being locked or unlocked as a result of player-sent packets
config.disallowedLockRefIds = {}

-- Prevent certain object refIds from being trapped or untrapped as a result of player-sent packets
config.disallowedTrapRefIds = {}

-- Prevent certain object refIds from being enabled or disabled as a result of player-sent packets
config.disallowedStateRefIds = {"listien bierles", "ulyne henim", "marcel maurard"}

-- Prevent save certain npc kills, because FL has Rebirth -- plugin that will resurrect NPC, so NPC on our server is immortals
config.ignoreKills = {
		"almalexia",
		"BM_horker_swim_UNIQUE",
		"dwarven ghost_radac",
		--"vivec_god",
		"yagrum bagam",
		"addhiranirr",
		"apelles matius",
		"asciene rane",
		"athyn sarethi",
		"berenziah",
		"Blatta Hateria",
		"brara morvayn",
		"caius cosades",
		"crassius cuiro",
		"crazy_batou",
		"danso indules",
		"divayth fyr",
		"dram bero",
		"dutadalk",
		"effe_tei",
		"endryn liethan",
		"falura llervu",
		"falx carius",
		"fedris hler",
		"garisa llethri",
		"gavas drin",
		"gilvas barelo",
		"han-ammu",
		"haspat antabolis",
		"hassour zainsubani",
		"hlaren ramoran",
		"huleeya",
		"karrod",
		"kaushad",
		"kausi",
		"King Hlaalu Helseth",
		"lord cluttermonkey",
		"manirai",
		"mehra milo",
		"miner arobar",
		"nevena ules",
		"nibani maesa",
		"raesa pullia",
		"savile imayn",
		"sharn gra-muzgob",
		"sinnamu mirpal",
		"sonummu zabamat",
		"sul-matuul",
		"tharsten heart-fang",
		"tholer saryoni",
		"Tienius Delitian",
		"uupse fyr",
		"varvur sarethi",
		"velanda omani",
		"yenammu"
	}
	
-- Cursed items, used to replace item to uncursed when player take them
-- table: cursed item, uncursed item
config.cursedItems = {
	["ingred_cursed_daedras_heart_01"] = "ingred_daedras_heart_01",
	["ingred_dae_cursed_diamond_01"] = "ingred_diamond_01",
	["ebony broadsword_dae_cursed"] = "ebony broadsword",
	["ingred_dae_cursed_emerald_01"] = "ingred_emerald_01",
	["fiend spear_dae_cursed"] = "fiend spear",
	["glass dagger_dae_cursed"] = "glass dagger",
	["imperial helmet armor_dae_curse"] = "imperial helmet armor",
	["ingred_dae_cursed_pearl_01"] = "ingred_pearl_01",
	["ingred_dae_cursed_raw_ebony_01"] = "ingred_raw_ebony_01",
	["ingred_dae_cursed_ruby_01"] = "ingred_ruby_01",
	["light_com_dae_cursed_candle_10"] = "light_com_candle_10",
	["gold_dae_cursed_001"] = "gold_001",
	["gold_dae_cursed_005"] = "gold_005",
	["silver dagger_hanin cursed"] = "Silver Dagger_Hanin",
	["misc_dwrv_cursed_coin00"] = "misc_dwrv_coin00",
	["ebony broadsword_dae_cursed"] = "ebony broadsword"
}

-- Prevent object scales from being set this high
config.maximumObjectScale = 20

-- The prefix used for automatically generated record IDs
-- Note 1: Records with automatically generated IDs get erased when there are no more instances of
-- them in player inventories/spellbooks or in cells
-- Note 2: By default, records created through regular gameplay (i.e. player-created spells, potions,
-- enchantments and enchanted items) use automatically generated record IDs, as do records created
-- via the /createrecord command when no ID is specified there
config.generatedRecordIdPrefix = "$custom"

-- The types of record stores used on this server in the order in which they should be loaded for
-- players, with the correct order ensuring that enchantments are loaded before items that might be
-- using those enchantments or ensuring that NPCs are loaded after the items they might have in their
-- inventories
config.recordStoreLoadOrder = { "spell", "potion", "enchantment", "armor", "book", "clothing", "weapon",
    "miscellaneous", "creature", "npc" }

-- The types of records that can be enchanted and therefore have links to enchantment records
config.enchantableRecordTypes = { "armor", "book", "clothing", "weapon" }

-- The types of records that can be stored by players and therefore have links to players,
-- listed in the order in which they should be loaded
config.carriableRecordTypes = { "spell", "potion", "armor", "book", "clothing", "weapon", "miscellaneous" }

-- The settings which are accepted as input for different record types when using /storerecord
config.validRecordSettings = {
    armor = { "baseId", "id", "name", "model", "icon", "script", "enchantmentId", "enchantmentCharge",
        "subtype", "weight", "value", "health", "armorRating" },
    book = { "baseId", "id", "name", "model", "icon", "script", "enchantmentId", "enchantmentCharge",
        "text", "weight", "value", "scrollState", "skillId" },
    clothing = { "baseId", "id", "name", "model", "icon", "script", "enchantmentId", "enchantmentCharge",
        "subtype", "weight", "value" },
    creature = { "baseId", "id", "name", "model", "script", "subtype", "level", "health", "magicka",
        "fatigue", "aiFight", "flags" },
    enchantment = { "baseId", "id", "subtype", "cost", "charge", "autoCalc", "effects" },
    miscellaneous = { "baseId", "id", "name", "model", "icon", "script", "weight", "value", "keyState" },
    npc = { "baseId", "inventoryBaseId", "id", "name", "script", "flags", "gender", "race", "model", "hair",
        "head", "class", "faction", "level", "health", "magicka", "fatigue", "aiFight", "autoCalc" },
    potion = { "baseId", "id", "name", "model", "icon", "script", "weight", "value", "autoCalc" },
    spell = { "baseId", "id", "name", "subtype", "cost", "flags", "effects" },
    weapon = { "baseId", "id", "name", "model", "icon", "script", "enchantmentId", "enchantmentCharge",
        "subtype", "weight", "value", "health", "speed", "reach", "damageChop", "damageSlash", "damageThrust",
        "flags" }
}

-- The settings which need to be provided when creating a new record that isn't based at all
-- on an existing one, i.e. a new record that is missing a baseId
config.requiredRecordSettings = {
    armor = { "name", "model" },
    book = { "name", "model" },
    clothing = { "name", "model" },
    creature = { "name", "model" },
    enchantment = {},
    miscellaneous = { "name", "model" },
    npc = { "name", "race", "class" },
    potion = { "name", "model" },
    spell = { "name" },
    weapon = { "name", "model" }
}

-- The record type settings whose input should be converted to numerical values when using /storerecord
config.numericalRecordSettings = { "subtype", "weight", "value", "cost", "charge", "health", "armorRating",
    "speed", "reach", "level", "magicka", "fatigue", "aiFight", "autoCalc", "gender", "flags", "enchantmentCharge" }

-- The record type settings whose input should be converted to booleans when using /storerecord
config.booleanRecordSettings = { "scrollState", "keyState" }

-- The record type settings whose input should be converted to tables with a min and a max numerical value
config.minMaxRecordSettings = { "damageChop", "damageSlash", "damageThrust" }

-- The types of object and actor packets stored in cell data
config.cellPacketTypes = { "delete", "place", "spawn", "lock", "trap", "scale", "state", "doorState",
    "container", "equipment", "ai", "death", "actorList", "position", "statsDynamic", "cellChangeTo",
    "cellChangeFrom" }

-- Whether the server should enforce that all clients connect with a specific list of plugins
-- defined in data/pluginlist.json
-- Warning: Only set this to false if you trust the people connecting and are sure they know
--          what they're doing. Otherwise, you risk getting corrupt server data from
--          their usage of unshared plugins.
config.enforcePlugins = true

-- Whether the server should avoid crashing when Lua script errors occur
-- Warning: Only set this to true if you want to have a highly experimental server where
--          important data can potentially stay unloaded or get overwritten
config.ignoreScriptErrors = false

-- The type of database or data format used by the server
-- Valid values: json, sqlite3
-- Note: The latter is only partially implemented as of now
config.databaseType = "json"

-- The location of the database file
-- Note: Not applicable when using json
config.databasePath = tes3mp.GetDataPath() .. "/database.db" -- Path where database is stored

-- Disallow players from including the following in their own names or the names of their custom items
-- Note: Unfortunately, these are based on real names that trolls have been using on servers
config.disallowedNameStrings = { "bitch", "blowjob", "blow job", "cocksuck", "cunt", "ejaculat",
    "faggot", "fellatio", "fuck", "gas the ", "Hitler", "jizz", "nigga", "nigger", "smegma", "vagina", "whore", "хуй", "гитлер", "навэльный", "навальный",
    "путин", "путлер", "бандера", "парашенко" }

-- The order in which table keys should be saved to JSON files
config.playerKeyOrder = { "login", "settings", "character", "customClass", "location", "stats",
    "fame", "shapeshift", "attributes", "attributeSkillIncreases", "skills", "skillProgress",
    "recordLinks", "equipment", "inventory", "spellbook", "books", "factionRanks", "factionReputation",
    "factionExpulsion", "mapExplored", "ipAddresses", "customVariables", "admin", "difficulty",
    "enforcedLogLevel", "physicsFramerate", "consoleAllowed", "bedRestAllowed",
    "wildernessRestAllowed", "waitAllowed", "gender", "race", "head", "hair", "class", "birthsign",
    "cell", "posX", "posY", "posZ", "rotX", "rotZ", "healthBase", "healthCurrent", "magickaBase",
    "magickaCurrent", "fatigueBase", "fatigueCurrent", "kills" }

config.cellKeyOrder = { "packets", "entry", "lastVisit", "recordLinks", "objectData", "refId", "count",
    "charge", "enchantmentCharge", "location", "actorList", "ai", "summon", "stats", "cellChangeFrom",
    "cellChangeTo", "container", "death", "delete", "doorState", "equipment", "inventory", "lock",
    "place", "position", "scale", "spawn", "state", "statsDynamic", "trap" }

config.recordstoreKeyOrder = { "general", "permanentRecords", "generatedRecords", "recordLinks",
    "id", "baseId", "name", "subtype", "gender", "race", "hair", "head", "class", "faction", "cost",
    "value", "charge", "weight", "autoCalc", "flags", "icon", "model", "script", "attribute", "skill",
    "rangeType", "area", "duration", "magnitudeMax", "magnitudeMin", "effects", "players", "cells", "global" }

config.worldKeyOrder = { "general", "time", "customVariables", "year", "month", "day", "hour", "daysPassed", "timeScale" }

return config
