fixme = require("fixme")
controlPanel = require("controlPanel")
character = require("character")

local commandHandler = {}

function commandHandler.ProcessCommand(pid, cmd)

    if cmd[1] == nil then
        Players[pid]:Message("// #ff0000" .. tr("Please use a command after the / symbol.\n"))
        return false
    else
        -- The command itself should always be lowercase
        cmd[1] = string.lower(cmd[1])
    end

    local admin = false
    local ordinator = false
    local moderator = false

    if Players[pid]:IsAdmin() then
        admin = true
        ordinator = true
        moderator = true
    elseif Players[pid]:IsOrdinator() then
        ordinator = true
        moderator = true
    elseif Players[pid]:IsModerator() then
        moderator = true
    end

	--= Player =--
	-- RP
    if cmd[1] == "message" or cmd[1] == "msg" or cmd[1] == "w" or cmd[1] == "pm" then
		if cmd[2] == nil then
			Players[pid]:Message("// #ff0000Неверный ввод, используйте /" .. cmd[1] .. " <pid> <сообщение> или /" .. cmd[1] .. " \"игрок\" <сообщение>, указание кавычек при использовании имени игрока обязательно!\n")
		else
			if string.sub(cmd[2], 1, 1) == "\"" then
				chatHelper.PlayerPrivateMessageToName(pid, table.join(cmd, " ", 2))
			else
				chatHelper.PlayerPrivateMessage(pid, tonumber(cmd[2]), table.join(cmd, " ", 3))
			end
		end

    elseif cmd[1] == "me" then
		chatHelper.PlayerMeCommand(pid, table.join(cmd, " ", 2))
        
    elseif cmd[1] == "globalchat" and moderator then
		if cmd[3] ~= nil then
			chatHelper.GlobalChatAdmin(pid, cmd[2], cmd[3])
		else
			chatHelper.GlobalChatAdmin(pid, cmd[2])
		end
		
	elseif cmd[1] == "mute" and moderator then
		chatHelper.Mute(pid, tonumber(cmd[2]))
		
	elseif cmd[1] == "do" then
		chatHelper.PlayerDoCommand(pid, table.join(cmd, " ", 2))
	
	elseif cmd[1] == "todo" then
		chatHelper.PlayerTodoCommand(pid, table.join(cmd, " ", 2))
		
	elseif cmd[1] == "try" then
		chatHelper.PlayerTryCommand(pid, table.join(cmd, " ", 2))
		
	elseif cmd[1] == "a" then
		chatHelper.SendGlobalMessage(pid, table.join(cmd, " ", 2))
		
	elseif cmd[1] == "chatopt" then
		if cmd[2] ~= nil and (cmd[2] == "global" or cmd[2] == "local") then
			chatHelper.OnPlayerCommand(pid, cmd[2])
		else
			chatHelper.ChatOptionsDialog(pid)
		end
	
	-- helpers
	elseif cmd[1] == "fixme" then
		if config.allowFixme == true then
            fixme.FixMe(pid)
        else
            Players[pid]:Message("// #ff0000" .. tr("That command is disabled on this server.\n"))
        end
        
	    elseif (cmd[1] == "anim" or cmd[1] == "a") and cmd[2] ~= nil then
        local isValid = animHelper.PlayAnimation(pid, cmd[2])

        if not isValid then
            local validList = animHelper.GetValidList(pid)
            Players[pid]:Message("// #ff0000" .. tr("That is not a valid animation. Try one of the following:\n") ..
                validList .. "\n")
        end

    elseif (cmd[1] == "speech" or cmd[1] == "s") and cmd[2] ~= nil and cmd[3] ~= nil and
        tonumber(cmd[3]) ~= nil then
        local isValid = speechHelper.PlaySpeech(pid, cmd[2], tonumber(cmd[3]))

        if not isValid then
            local validList = speechHelper.GetValidList(pid)
            Players[pid]:Message(tr("That is not a valid speech. Try one of the following:\n")
                .. validList .. "\n")
        end
        
	elseif cmd[1] == "suicide" then
        if config.allowSuicideCommand == true then
            tes3mp.SetHealthCurrent(pid, 0)
            tes3mp.SendStatsDynamic(pid)
        else
            Players[pid]:Message("// #ff0000" .. tr("That command is disabled on this server.\n"))
        end
        
	elseif cmd[1] == "players" or cmd[1] == "list" then
        guiHelper.ShowPlayerList(pid)

    elseif cmd[1] == "help" then
        ShowMenu(pid)
        
	--= Admin =--
	-- Character
    elseif cmd[1] == "setattr" and admin then
		character.SetAttribute(tonumber(cmd[2]), tonumber(cmd[3]), tonumber(cmd[4]))

    elseif cmd[1] == "setskill" and ordinator then
		character.SetSkill(tonumber(cmd[2]), tonumber(cmd[3]), tonumber(cmd[4]))
		
	elseif cmd[1] == "setscale" and admin then
		character.SetScale(pid, tonumber(cmd[2]), tonumber(cmd[3]))

    elseif cmd[1] == "disguise" and admin then
		character.Disguise(pid, tonumber(cmd[2]), table.join(cmd, " ", 3))

    elseif cmd[1] == "usecreaturename" and admin then
		character.UseCreatureName(pid, tonumber(cmd[2]), cmd[3])

	elseif cmd[1] == "getkills" and ordinator then
		controlPanel.GetKills(pid, tonumber(cmd[2]))
	
	elseif cmd[1] == "setkill" and ordinator then
		controlPanel.SetKill(pid, tonumber(cmd[2]), tonumber(cmd[3]), table.join(cmd, " ", 4))
		
	elseif cmd[1] == "setquestindex" and ordinator then
		controlPanel.SetQuestIndex(pid, tonumber(cmd[2]), tonumber(cmd[3]), table.join(cmd, " ", 4))
	
	elseif cmd[1] == "deletequest" and ordinator then
		controlPanel.SetQuestIndex(pid, tonumber(cmd[2]), table.join(cmd, " ", 4))
		
	elseif cmd[1] == "getfactionexp" and ordinator then
		controlPanel.GetFactionExpulsion(pid, tonumber(cmd[2]))
		
	elseif cmd[1] == "setfactionexp" and ordinator then
		controlPanel.SetFactionExpulsion(pid, tonumber(cmd[2]), cmd[3], cmd[4])
		
	elseif cmd[1] == "allowedkillplayers" and ordinator then
		controlPanel.SetAllowedKillPlayers(pid, tonumber(cmd[2]), cmd[3])

	-- helpers
	elseif cmd[1] == "removeproxy" and ordinator then
		ProxyFucker.RemoveProxy(pid, cmd[2])
	
	elseif cmd[1] == "restart" and ordinator then
		controlPanel.RestartServer(pid)
	
	elseif cmd[1] == "cellinfo" and ordinator then
		Rebirth.CellInfo(pid)
	
	elseif cmd[1] == "respawn" and ordinator then
		Rebirth.ForceRespawn(pid)
	
    elseif cmd[1] == "ban" and ordinator then
		controlPanel.Ban(pid, tonumber(cmd[2]))

    elseif cmd[1] == "unban" and ordinator then
		controlPanel.Unban(pid, table.join(cmd, " ", 2))

    elseif cmd[1] == "banlist" and ordinator then
		controlPanel.Banlist(pid, table.join(cmd, " ", 2))

    elseif cmd[1] == "cells" and ordinator then
        guiHelper.ShowCellList(pid)

    elseif cmd[1] == "regions" and ordinator then
        guiHelper.ShowRegionList(pid)

    elseif (cmd[1] == "teleport" or cmd[1] == "tp") and ordinator then
		controlPanel.Teleport(pid, cmd[2])

    elseif (cmd[1] == "teleportto" or cmd[1] == "tpto") and ordinator then
        logicHandler.TeleportToPlayer(pid, pid, cmd[2])

    elseif (cmd[1] == "setauthority" or cmd[1] == "setauth") and admin and #cmd > 2 then
		controlPanel.SetAuthority(pid, tonumber(cmd[2]), table.join(cmd, " ", 3))

    elseif cmd[1] == "kick" and ordinator then
		controlPanel.Kick(pid, tonumber(cmd[2]))

    elseif cmd[1] == "addordinator" and admin then
		controlPanel.AddOrdinator(pid, tonumber(cmd[2]))

    elseif cmd[1] == "removeordinator" and admin then
		controlPanel.RemoveOrdinator(pid, tonumber(cmd[2]))

    elseif cmd[1] == "addmoderator" and admin then
		controlPanel.AddModerator(pid, tonumber(cmd[2]))

    elseif cmd[1] == "removemoderator" and admin then
		controlPanel.RemoveModerator(pid, tonumber(cmd[2]))
		
    elseif cmd[1] == "setext" and admin then
        tes3mp.SetExterior(pid, cmd[2], cmd[3])

    elseif (cmd[1] == "setdifficulty" or cmd[1] == "setdiff") and admin then
		controlPanel.SetDifficulty(pid, tonumber(cmd[2]), cmd[3])

    elseif cmd[1] == "setconsole" and admin then
		controlPanel.SetConsole(pid, tonumber(cmd[2]), cmd[3])

    elseif cmd[1] == "setbedrest" and admin then
		controlPanel.SetBedRest(pid, tonumber(cmd[2]), cmd[3])

    elseif (cmd[1] == "setwildernessrest" or cmd[1] == "setwildrest") and admin then
        controlPanel.SetWildRest(pid, tonumber(cmd[2]), cmd[3])

    elseif cmd[1] == "setwait" and admin then
        controlPanel.SetWait(pid, tonumber(cmd[2]), cmd[3])
        
    elseif (cmd[1] == "setphysicsfps" or cmd[1] == "setphysicsframerate") and admin then
		controlPanel.SetPhysicsFPS(pid, tonumber(cmd[2]), tonumber(cmd[3]))

    elseif (cmd[1] == "setloglevel" or cmd[1] == "setenforcedloglevel") and admin then
		controlPanel.SetLogLevel(pid, tonumber(cmd[2]), tonumber(cmd[3]))

    elseif cmd[1] == "sethour" and admin then
		controlPanel.SetHour(pid, cmd[2])

    elseif cmd[1] == "setday" and admin then
		controlPanel.SetDay(pid, cmd[2])

    elseif cmd[1] == "setmonth" and admin then
		controlPanel.SetMonth(pid, cmd[2])

    elseif cmd[1] == "settimescale" and admin then
		controlPanel.SetTimeScale(pid, cmd[2])

    elseif cmd[1] == "setcollision" and admin then
		controlPanel.SetCollition(pid, cmd[2], cmd[3])

    elseif cmd[1] == "overridecollision" and admin then
		controlPanel.OverrideCollision(pid, cmd[2], cmd[3])

    elseif (cmd[1] == "placeat" or cmd[1] == "spawnat") and ordinator then
		controlPanel.PlaceAt(pid, tonumber(cmd[2]), table.join(cmd, " ", 3), cmd[1])

    elseif cmd[1] == "confiscate" and ordinator then
		controlPanel.Confiscate(pid, tonumber(cmd[2]))
		
	elseif cmd[1] == "kill" and ordinator then
		controlPanel.Kill(pid, tonumber(cmd[2]))
		
	elseif cmd[1] == "restorehealth" and admin then
		Players[tonumber(cmd[2])]:RestoreHealth()
		tes3mp.SendStatsDynamic(tonumber(cmd[2]))
		
	elseif cmd[1] == "testdoor" and admin then
		tes3mp.SetObjectRefNum(57327)
		tes3mp.SetObjectMpNum(0)
		tes3mp.SetObjectDoorDestinationCell(table.join(cmd, " ", 2))
		tes3mp.SetObjectDoorTeleportState(true)
		tes3mp.AddObject()
		tes3mp.SendDoorDestination(false, false)
		
	-- custom
	elseif commandInterface.Validate(cmd) then
		commandInterface.CommandCallback(pid, cmd, cmd[1])
        
    else
        Players[pid]:Message("// #ff0000" .. tr("Not a valid command. Type /help for more info.\n"))
    end
end

return commandHandler
