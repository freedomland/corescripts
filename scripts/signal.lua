local objects = {
	["OnPlayerResurrect"] = {},
	["OnPlayerActivate"] = {},
	["OnPlayerFinishLogin"] = {},
	["OnPlayerDisconnect"] = {},
	["OnPlayerSendMessage"] = {},
	["OnPlayerDeath"] = {},
	["OnPlayerChargenEnded"] = {},
	["OnPlayerConnect"] = {},
	["OnSpellAdd"] = {},
	["OnSpellRemove"] = {},
	
	["OnCellLoad"] = {},
	["OnPlayerCellChange"] = {},
	["OnLoginTimeExpiration"] = {},
	
	["OnServerExit"] = {},
	["OnServerCrash"] = {},

	-- special events
	["OnActorDeath"] = {},
	["OnObjectActivate"] = {},
	["OnObjectDelete"] = {},
	["OnDisallowObjectDelete"] = {},
	["OnObjectPlace"] = {},
	["OnItemUse"] = {},
	["OnJournal"] = {}
}

function connect(eventString, func)
	if objects[eventString] == nil then
		tes3mp.LogMessage(enumerations.log.INFO, string.format("- Can't register %s in event %s. Abort.", func, eventString))
		return
	end
	
	if objects[eventString][func] ~= nil then
		tes3mp.LogMessage(enumerations.log.INFO, string.format("- %s in event %s already registered. Abort.", func, eventString))
		return
	end
	
	table.insert(objects[eventString], func)
end

function emit(eventString, args)
	for ind, func in pairs(objects[eventString]) do
		func(unpack(args))
	end
	
	tes3mp.LogMessage(enumerations.log.INFO, "- Calling event " .. eventString .. ".")
end

-- TODO: get rid of it? I couldn't think of anything better
function connectSpecial(eventString, refId, func)
	if objects[eventString] == nil then
		tes3mp.LogMessage(enumerations.log.INFO, "- Can't register special event of " .. eventString .. ". Abort.")
		return
	end
	
	if objects[eventString][refId] ~= nil then
		tes3mp.LogMessage(enumerations.log.INFO, "- Special event of " .. refId .. " already registered. Abort.")
		return
	end
	
	objects[eventString][refId] = func
end

-- Only emit if refId is the same
function emitSpecial(eventString, refId, args)
	if objects[eventString][refId] == nil then
		return false
	else
		tes3mp.LogMessage(enumerations.log.INFO, string.format("- Calling event %s for %s.", eventString, refId))
		objects[eventString][refId](unpack(args))
		return true
	end
end

function SendObjectActivate()
	tes3mp.CopyReceivedObjectListToStore()
	tes3mp.SendObjectActivate(false, false)
end

function SendObjectDelete()
	tes3mp.CopyReceivedObjectListToStore()
	tes3mp.SendObjectDelete(true, false)
end

function SendObjectPlace()
	tes3mp.CopyReceivedObjectListToStore()
	tes3mp.SendObjectPlace(true, false)
end
