# CoreScripts

Fork of https://github.com/TES3MP/CoreScripts .

### Requirements

- TES3MP at least 0.7
- [Translation System](https://gitlab.com/freedomland/translation-system) (will be removed soon, we don't have resources to support both versions of game, so we stick with Russian version, sorry)
- Linux, we don't want to support Wendoz and never will support Wendoz. If you want to host your highly loaded server on Wendoz you should think twice.
- [luaLS](https://gitlab.com/freedomland/luafs)

### Why?

Well, you know. tes3mp is not for multiplayer.

Short story long: I was frustrated of default plugin system since 0.6.2-hotfixed. Ok, you can disable most sharing function so it will be (mostly) multiplayer-based plugins. BUT, you can't disable killcount -- it will be always worldwide. It break most quests, when you need kill someone, or someone even kill, for example, Fyr and newcomers can't even take their first quest, because Kai will send they away. This is deadly thing for multiplayer, you CAN'T clear this value every now and then.

Second thing I noticed -- moderators have too much powers. Like srsl, moders can teleport, ban, kick, and everything. What the point to have admins then?

Missing functions: for multiplayer you expect more functions, like seriosly -- can you mute players in chat? No? Well, now you can.

**Warning:** We're now stick with CJSON and it doesn't like comments in json files. We're remove checks for this, because we want (and need) to write and read json fast.

Please, read [wiki](https://gitlab.com/freedomland/corescripts/wikis/home), it will explaing everything you want to know.

### Changes from original

- Do not delete Socucius Ergalla, so we can make quests from him (this requires esp to remove all chargen scripts)
- Rewrite fixme, because it's was buggy
- Add some [new colors](assets/colors.png)
- Kills split from world and apply it to specific player
- New api for world class, killed npcs now saved to world table, so they can be resurrected later
- Now you can controll all skills\attributes cup separatly through config.lua
- Uncursed build in: when player take item it will be replaced with uncursed version

- Split the stuff rank
Now you have the 3 ranks for stuff with different ability to doing something:
1. Moderator - only have ability to controll chat, mute\unmute player, disable\enable global chat for everybody or for specific player.
2. Ordinator - it's a person that you trust the most. He can controll almost everything, but he can't controll console and some specific things. He still can spawn NPC, objects and teleports players. He can also controll players skills.
3. Admin - The God (ex. Server Owner). Can do everything.

### Function changes

- IsServerOwner() is now depricated, I found this function frustrated, use IsAdmin() instead (see above)
- Fixme is now in it's own file
- Command handler now only call functions from another classes
- New class: chat.lua that allow you to controll almost all chat-related functions
- character.lua now holds all character-related functions, such as shapeswitch
- controlPanel.lua now holds all admin commands

Add new commands:

- /msg <nick> <message> - use nick instead of pid
- /globalchat [pid] on/off - disable or enable global chat
- /mute <pid> on/off - mute or unmute specific player
- /do, /todo, /try - everything as in "big" MMORPG now in tes3mp

### Why menuHelper.lua was removed?

Let me be honest. It's not useful. When it's first came out I was already frustrated of it's complexity. Menus itself is frustrating and lack of functions. It doesn't used for anyting than stupid help menu, because it's too complicated. I suggest you to use [mpWidgets](https://gitlab.com/freedomland/mpwidgets) instead.
